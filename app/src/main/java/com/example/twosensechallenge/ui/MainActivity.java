package com.example.twosensechallenge.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.twosensechallenge.R;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		View simpleButton = findViewById(R.id.simpleGestureButton);
		View advancedButton = findViewById(R.id.advancedGestureButton);

		simpleButton.setOnClickListener(this);
		advancedButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		startActivity(view.getId() == R.id.advancedGestureButton ? new Intent(this, AdvancedGestureActivity.class) : new Intent(this, SimpleGestureActivity.class));
	}
}
