package com.example.twosensechallenge;

import android.app.Application;

import timber.log.Timber;

public class TwoSenseChallengeApp extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		if (BuildConfig.DEBUG) Timber.plant(new Timber.DebugTree());
	}
}
