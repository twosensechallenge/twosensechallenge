package com.example.twosensechallenge.gestures.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;

import com.example.twosensechallenge.gestures.model.TouchGesture;

import androidx.annotation.Nullable;
import timber.log.Timber;

public class TouchGestureView extends View {

	private static final int VELOCITY_UNITS = 1000; //pixels per second
	private float eventX, historicalX, velocityX;
	private float eventY, historicalY, velocityY;
	private int mSwipeThreshold;
	private int mSwipeVelocity;
	private int mLongPressTimeOut;
	private boolean mHasActiveTapTracker;
	private VelocityTracker mVelocityTracker;
	private onEventListener mListener;
	private final Handler mHandler = new Handler();
	private final Runnable mLongPressRunner = this::onLongPressDetected;

	public TouchGestureView(Context context) {
		super(context);
		initThresholds(context);
	}

	public TouchGestureView(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		initThresholds(context);
	}

	public TouchGestureView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initThresholds(context);
	}

	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public TouchGestureView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		initThresholds(context);
	}

	private void initThresholds(Context context) {
		mSwipeThreshold = ViewConfiguration.get(context).getScaledTouchSlop();
		mSwipeVelocity = ViewConfiguration.get(context).getScaledMinimumFlingVelocity();
		mLongPressTimeOut = ViewConfiguration.getLongPressTimeout();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);

		int pointerId = event.getPointerId(event.getActionIndex());

		switch (event.getAction()) {
			case MotionEvent.ACTION_MOVE:
				eventX = event.getX();
				eventY = event.getY();
				calculateVelocity(event, pointerId);
				onPossibleSwipeDetected();
				break;
			case MotionEvent.ACTION_UP:
				clearVelocityTracker();
				clearLongPressTracker();
				if (hasActiveTapTracker()) {
					onTapDetected();
					performClick(); // enables the accessibility service to perform click for a user who cannot click the screen.
					clearTapTracker();
				}
				break;
			case MotionEvent.ACTION_DOWN:
				historicalX = event.getX();
				historicalY = event.getY();
				startVelocityTracker(event);
				startTapTracker();
				startLongPressTracker(event);
				break;
			case MotionEvent.ACTION_CANCEL:
				clearVelocityTracker();
				clearTapTracker();
				clearLongPressTracker();
				break;
			default:
				return false;
		}
		return true;
	}

	@Override
	public boolean performClick() {
		// calls the super implementation to generates the AccessibilityEvent
		super.performClick();
		return true;
	}

	private void onTapDetected() {
		Timber.d("onTapDetected() called");
		if (mListener != null) mListener.onGestureDetected(TouchGesture.TAP);
	}

	private void onLongPressDetected() {
		Timber.d("onLongPressDetected() called");
		clearTapTracker();
		if (mListener != null) mListener.onGestureDetected(TouchGesture.LONG_PRESS);
	}

	private void onPossibleSwipeDetected() {
		float diffY = eventY - historicalY;
		float diffX = eventX - historicalX;
		if (Math.abs(diffX) > Math.abs(diffY)) {
			if (Math.abs(diffX) > mSwipeThreshold && Math.abs(velocityX) > mSwipeVelocity) {
				onSwipeDetected(diffX < 0 ? TouchGesture.SWIPE_LEFT : TouchGesture.SWIPE_RIGHT);
			}
		} else if (Math.abs(diffY) > mSwipeThreshold && Math.abs(velocityY) > mSwipeVelocity) {
			onSwipeDetected(diffY < 0 ? TouchGesture.SWIPE_UP : TouchGesture.SWIPE_DOWN);
		}
	}

	private void onSwipeDetected(TouchGesture touchGesture) {
		Timber.d("onSwipeDetected() called: %s ", touchGesture.name());
		clearLongPressTracker();
		clearTapTracker();
		if (mListener != null) mListener.onGestureDetected(touchGesture);
	}

	private void startLongPressTracker(MotionEvent event) {
		mHandler.postAtTime(mLongPressRunner, event.getDownTime() + mLongPressTimeOut);
	}

	private void startTapTracker() {
		mHasActiveTapTracker = true;
	}

	private boolean hasActiveTapTracker() {
		return mHasActiveTapTracker;
	}

	private void clearTapTracker() {
		mHasActiveTapTracker = false;
	}

	private void clearLongPressTracker() {
		mHandler.removeCallbacks(mLongPressRunner);
	}

	private void clearVelocityTracker() {
		if (mVelocityTracker != null) {
			mVelocityTracker.recycle();
			mVelocityTracker = null;
		}
	}

	private void startVelocityTracker(MotionEvent event) {
		if (mVelocityTracker == null) {
			mVelocityTracker = VelocityTracker.obtain();
		} else {
			mVelocityTracker.clear();
		}
		mVelocityTracker.addMovement(event);
	}

	private void calculateVelocity(MotionEvent event, int pointerId) {
		mVelocityTracker.addMovement(event);
		mVelocityTracker.computeCurrentVelocity(VELOCITY_UNITS);
		velocityX = mVelocityTracker.getXVelocity(pointerId);
		velocityY = mVelocityTracker.getYVelocity(pointerId);
	}

	public void setEventListener(onEventListener listener) {
		this.mListener = listener;
	}

	public interface onEventListener {
		void onGestureDetected(TouchGesture touchGesture);
	}
}