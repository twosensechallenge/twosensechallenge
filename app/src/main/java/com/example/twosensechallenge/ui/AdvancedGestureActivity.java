package com.example.twosensechallenge.ui;

import android.os.Bundle;
import android.widget.TextView;

import com.example.twosensechallenge.gestures.view.TouchGestureView;
import com.example.twosensechallenge.R;

import androidx.appcompat.app.AppCompatActivity;

public class AdvancedGestureActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gesture);

		TextView actionText = findViewById(R.id.actionText);
		TouchGestureView touchView = findViewById(R.id.touchView);

		touchView.setEventListener(touchGesture -> {
			switch (touchGesture) {
				case TAP:
					actionText.setText(R.string.event_name_tap);
					break;
				case LONG_PRESS:
					actionText.setText(R.string.event_name_long_press);
					break;
				case SWIPE_UP:
					actionText.setText(R.string.event_name_swipe_up);
					break;
				case SWIPE_DOWN:
					actionText.setText(R.string.event_name_swipe_down);
					break;
				case SWIPE_LEFT:
					actionText.setText(R.string.event_name_swipe_left);
					break;
				case SWIPE_RIGHT:
					actionText.setText(R.string.event_name_swipe_right);
					break;
				default:
					actionText.setText(R.string.event_name_none);
			}
		});
	}
}
