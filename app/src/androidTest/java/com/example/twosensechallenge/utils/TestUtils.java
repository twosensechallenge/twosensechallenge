package com.example.twosensechallenge.utils;

import com.example.twosensechallenge.R;

import androidx.test.espresso.ViewAction;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class TestUtils {

	public static void doTap() {
		perform(click());
	}

	public static void doLongPress() {
		perform(longClick());
	}

	public static void doSwipeLeft() {
		perform(swipeLeft());
	}

	public static void doSwipeUp() {
		perform(swipeUp());
	}

	public static void doSwipeRight() {
		perform(swipeRight());
	}

	public static void doSwipeDown() {
		perform(swipeDown());
	}

	public static void validateActionText(int expectedStringResource) {
		onView(withId(R.id.actionText)).check(matches(withText(expectedStringResource)));
	}

	private static void perform(final ViewAction... viewActions) {
		onView(withId(R.id.touchView)).perform(viewActions);
	}

}
