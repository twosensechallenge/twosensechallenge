package com.example.twosensechallenge;

import com.example.twosensechallenge.ui.AdvancedGestureActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import androidx.test.espresso.ViewAction;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static com.example.twosensechallenge.utils.TestUtils.doSwipeDown;
import static com.example.twosensechallenge.utils.TestUtils.doSwipeLeft;
import static com.example.twosensechallenge.utils.TestUtils.doSwipeRight;
import static com.example.twosensechallenge.utils.TestUtils.doSwipeUp;
import static com.example.twosensechallenge.utils.TestUtils.validateActionText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class AdvancedGestureActivityTest {

	@Rule
	public ActivityTestRule<AdvancedGestureActivity> activityRule = new ActivityTestRule<>(AdvancedGestureActivity.class);

	@Before
	public void noAction() {
		validateActionText(R.string.event_name_none);
	}


	@Test
	public void testSwipeLeft() {
		doSwipeLeft();
		validateActionText(R.string.event_name_swipe_left);
	}

	@Test
	public void testSwipeRight() {
		doSwipeRight();
		validateActionText(R.string.event_name_swipe_right);
	}

	@Test
	public void testSwipeUp() {
		doSwipeUp();
		validateActionText(R.string.event_name_swipe_up);
	}

	@Test
	public void testSwipeDown() {
		doSwipeDown();
		validateActionText(R.string.event_name_swipe_down);
	}

	@Test
	public void randomMove() {
		Map<ViewAction, Integer> actionToNameMap = new HashMap<>();

		actionToNameMap.put(click(), R.string.event_name_tap);
		actionToNameMap.put(longClick(), R.string.event_name_long_press);
		actionToNameMap.put(swipeLeft(), R.string.event_name_swipe_left);
		actionToNameMap.put(swipeUp(), R.string.event_name_swipe_up);
		actionToNameMap.put(swipeRight(), R.string.event_name_swipe_right);
		actionToNameMap.put(swipeDown(), R.string.event_name_swipe_down);

		ViewAction[] values = actionToNameMap.keySet().toArray(new ViewAction[0]);

		for (int i = 0; i < 30; i++) {
			ViewAction randomMove = values[new Random().nextInt(values.length)];
			onView(withId(R.id.touchView)).perform(randomMove);
			Integer expectedStringResource = actionToNameMap.get(randomMove);
			validateActionText(expectedStringResource != null ? expectedStringResource : R.string.event_name_none);
		}
	}


}