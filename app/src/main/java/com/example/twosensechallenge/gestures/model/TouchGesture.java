package com.example.twosensechallenge.gestures.model;

public enum TouchGesture {
	TAP,
	LONG_PRESS,
	SWIPE_UP,
	SWIPE_DOWN,
	SWIPE_RIGHT,
	SWIPE_LEFT
}
