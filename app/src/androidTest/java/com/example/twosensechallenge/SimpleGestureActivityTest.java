package com.example.twosensechallenge;

import com.example.twosensechallenge.ui.SimpleGestureActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import androidx.test.espresso.ViewAction;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static com.example.twosensechallenge.utils.TestUtils.doLongPress;
import static com.example.twosensechallenge.utils.TestUtils.doSwipeDown;
import static com.example.twosensechallenge.utils.TestUtils.doSwipeLeft;
import static com.example.twosensechallenge.utils.TestUtils.doSwipeRight;
import static com.example.twosensechallenge.utils.TestUtils.doSwipeUp;
import static com.example.twosensechallenge.utils.TestUtils.doTap;
import static com.example.twosensechallenge.utils.TestUtils.validateActionText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class SimpleGestureActivityTest {

	@Rule
	public ActivityTestRule<SimpleGestureActivity> activityRule = new ActivityTestRule<>(SimpleGestureActivity.class);

	@Before
	public void noAction() {
		validateActionText(R.string.event_name_none);
	}

	@Test
	public void tap() {
		doTap();
		validateActionText(R.string.event_name_tap);
	}


	@Test
	public void randomContinuousTaps() {
		int randomNum = ThreadLocalRandom.current().nextInt(1, 5);
		for (int i = 0; i < randomNum; i++) {
			doTap();
			validateActionText(R.string.event_name_tap);
		}
	}


	@Test
	public void longPress() {
		doLongPress();
		validateActionText(R.string.event_name_long_press);
	}

	@Test
	public void randomContinuousLongPress() {
		int randomNum = ThreadLocalRandom.current().nextInt(1, 5);
		for (int i = 0; i < randomNum; i++) {
			doLongPress();
			validateActionText(R.string.event_name_long_press);
		}
	}

	@Test
	public void longPressAndTap() {
		doLongPress();
		validateActionText(R.string.event_name_long_press);

		doTap();
		validateActionText(R.string.event_name_tap);
	}

	@Test
	public void tapAndLongPress() {
		doTap();
		validateActionText(R.string.event_name_tap);

		doLongPress();
		validateActionText(R.string.event_name_long_press);
	}

	@Test
	public void swipe() {
		doSwipeLeft();
		validateActionText(R.string.event_name_swipe);

		doSwipeUp();
		validateActionText(R.string.event_name_swipe);

		doSwipeRight();
		validateActionText(R.string.event_name_swipe);

		doSwipeDown();
		validateActionText(R.string.event_name_swipe);
	}

	@Test
	public void randomMove() {
		Map<ViewAction, Integer> actionToNameMap = new HashMap<>();

		actionToNameMap.put(click(), R.string.event_name_tap);
		actionToNameMap.put(longClick(), R.string.event_name_long_press);
		actionToNameMap.put(swipeLeft(), R.string.event_name_swipe);
		actionToNameMap.put(swipeUp(), R.string.event_name_swipe);
		actionToNameMap.put(swipeRight(), R.string.event_name_swipe);
		actionToNameMap.put(swipeDown(), R.string.event_name_swipe);

		ViewAction[] values = actionToNameMap.keySet().toArray(new ViewAction[0]);

		for (int i = 0; i < 20; i++) {
			ViewAction randomMove = values[new Random().nextInt(values.length)];
			onView(withId(R.id.touchView)).perform(randomMove);
			Integer expectedStringResource = actionToNameMap.get(randomMove);
			validateActionText(expectedStringResource != null ? expectedStringResource : R.string.event_name_none);
		}
	}


	@Test
	public void tapSwipeLongPress() {
		doTap();
		validateActionText(R.string.event_name_tap);

		doSwipeUp();
		validateActionText(R.string.event_name_swipe);

		doLongPress();
		validateActionText(R.string.event_name_long_press);
	}


}