package com.example.twosensechallenge.ui;

import android.os.Bundle;
import android.widget.TextView;

import com.example.twosensechallenge.R;
import com.example.twosensechallenge.gestures.view.TouchGestureView;

import androidx.appcompat.app.AppCompatActivity;

public class SimpleGestureActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gesture);

		TextView actionText = findViewById(R.id.actionText);
		TouchGestureView touchView = findViewById(R.id.touchView);

		touchView.setEventListener(touchGesture -> {
			switch (touchGesture) {
				case TAP:
					actionText.setText(R.string.event_name_tap);
					break;
				case LONG_PRESS:
					actionText.setText(R.string.event_name_long_press);
					break;
				case SWIPE_UP:
				case SWIPE_DOWN:
				case SWIPE_LEFT:
				case SWIPE_RIGHT:
					actionText.setText(R.string.event_name_swipe);
					break;
				default:
					actionText.setText(R.string.event_name_none);
			}
		});
	}
}
